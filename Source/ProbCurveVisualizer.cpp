#include <JuceHeader.h>
#include "ProbCurveVisualizer.h"

//==============================================================================
ProbCurveVisualizer::ProbCurveVisualizer(ProbCurvAudioProcessor& p, AudioProcessorValueTreeState& _state)
    : processor(p), state(_state)
{
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.
    normalizedScaleFactor = .5;
    scaleFactor = 2;
    setFramesPerSecond(60);

    sampsTillNextNote = 0;

    curveTimescale = state.getRawParameterValue("curveTimescale");

    ringbuf = processor.getRingBuf();
    ringbufCopy.reserve(ringbuf->cap);
}

ProbCurveVisualizer::~ProbCurveVisualizer()
{
}

void ProbCurveVisualizer::mouseDown(const MouseEvent& e) {
    lastYOffset = 0;
}

void ProbCurveVisualizer::mouseDrag(const MouseEvent& e)
{
    float newOffset = e.getDistanceFromDragStartY();
    normalizedScaleFactor = jlimit<float>(0, 1, normalizedScaleFactor + ((newOffset - lastYOffset) / 100.));
    lastYOffset = newOffset;
}

void ProbCurveVisualizer::update() {

    float samplerate = processor.getSampleRate();
    sampsTillNextNote = processor.getSampsTillNextNote();

    //get ringbuf data
    ringbufCopy.clear();

    int start = ringbuf->start;
    int end = ringbuf->end;
    int size = ringbuf->cap + 1;

    //copy ringbuf data into local state
    while (start != end) {
        ringbufCopy.push_back(ringbuf->data[start]);
        //increment start
        if (++start == size) {
            if (end == size) {
                break;
            }
            start = 0;
        }
    }

    scaleFactor = NormalisableRange(.5f * *curveTimescale, 6 * *curveTimescale).convertFrom0to1(normalizedScaleFactor);
}

void ProbCurveVisualizer::paint (Graphics& g)
{
    //background
    g.setColour(Colours::black);
    g.fillRect(0, 0, getWidth(), getHeight());

    //draw frame
    g.setColour(Colours::grey);
    int vizMargin = 20;
    g.fillRect(vizMargin, getHeight() / 2, getWidth() - (2 * vizMargin), 1);
    g.drawLine(vizMargin, vizMargin, vizMargin, getHeight() - vizMargin);
    g.drawLine(getWidth() - vizMargin, vizMargin, getWidth() - vizMargin, getHeight() - vizMargin);

    //draw grid
    g.setColour(Colours::lightgrey);
    int mainGridTickHeight = (getHeight() - (2 * vizMargin)) * .7;
    //find grid tick width in seconds as a power of ten that will draw at least 1 lines
    float mainGridTickTime = pow(10, floor(log10(scaleFactor)));

    //if it draws more than five lines, draw every other line
    if (scaleFactor / mainGridTickTime > 5) {
        mainGridTickTime *= 2;
    }
    //if it only draws one line, draw twice as many lines
    else if (scaleFactor / mainGridTickTime < 2) {
        mainGridTickTime *= .5;
    }

    //convert from seconds to x coordinate
    float gridWidth = (getWidth() - 2 * vizMargin) / scaleFactor * mainGridTickTime;

    //don't draw a line for the 0s mark
    g.drawFittedText("0s", Rectangle(vizMargin, (getHeight() + mainGridTickHeight) / 2, 50, 20), Justification::left, 1);
    for (int i = 1; i < scaleFactor / mainGridTickTime; i++) {
        //draw main gridlines
        g.fillRect(vizMargin + int(gridWidth * i), (getHeight() - mainGridTickHeight) / 2, 1, mainGridTickHeight);
        
        //draw text
        char buffer[20];
        sprintf(buffer, "%gs", mainGridTickTime * i);
        g.drawFittedText(buffer, Rectangle(vizMargin + int(gridWidth * i), (getHeight() + mainGridTickHeight) / 2, 50, 20), Justification::left, 1);
    }

    //draw ticks for each upcoming pulse
    g.setColour(Colours::red);
    //xoffset from margin in pixels
    int noteTickX = vizMargin + (sampsTillNextNote / processor.getSampleRate() * getWidth()) / scaleFactor;
    //draw immediately next note (duration not changed when pulsebank refilled)
    if (noteTickX < getWidth() - vizMargin)
        g.fillRect(noteTickX, getHeight() / 2 - 15, 1, 30);

    for (int i = 0; i < ringbufCopy.size(); i++) {
        //render line
        noteTickX += (ringbufCopy[i] / processor.getSampleRate() * getWidth()) / scaleFactor;
        if (noteTickX < getWidth() - vizMargin) {
            g.fillRect(noteTickX, getHeight() / 2 - 15, 1, 30);
        }
        //don't calculate further renders if already offscreen
        else {
            break;
        }
    }
}

void ProbCurveVisualizer::resized()
{
}
