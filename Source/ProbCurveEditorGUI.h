/*
  ==============================================================================

    ProbCurveEditorGUI.h
    Created: 19 Feb 2020 5:33:17pm
    Author:  gsuml

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "ProbCurveEditorComponent.h"
#include "ProbCurveVisualizer.h"

//==============================================================================

class ProbCurveEditorGUI    : public Component
{
public:
    typedef AudioProcessorValueTreeState::SliderAttachment SliderAttachment;

    ProbCurveEditorGUI(ProbCurvAudioProcessor&, AudioProcessorValueTreeState&);
    ~ProbCurveEditorGUI();

    void paint (Graphics&) override;
    void resized() override;

private:
    ProbCurvAudioProcessor &processor;
    AudioProcessorValueTreeState& state;

    //numSliders: changes number of sliders in pceComponent
    //curveTimescale: length (in seconds) of maximum possible note duration
    Label numSlidersLabel, curveTimescaleLabel;
    Slider numSlidersSlider, curveTimescaleSlider;
    std::unique_ptr<SliderAttachment> numSlidersAttachment, curveTimescaleAttachment;

    //probability curve editor
    ProbCurveEditorComponent* pceComponent;
    ProbCurveVisualizer* pcViz;

    //initialize numSlidersSlider
    void makenumSlidersSlider();

    //initialize curveTimescaleSlider
    void makecurveTimescaleSlider();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ProbCurveEditorGUI)
};
