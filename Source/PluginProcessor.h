#pragma once

#include <JuceHeader.h>
#include <random>

class ProbCurvAudioProcessor  : public AudioProcessor
{
public:
    enum class OutputMode {
        chords = 0, //trigger all notes on each pulse
        arpeggiator = 1, //trigger one note on each pulse in sequence added to notebank
        random = 2 //trigger one random note on each pulse
    };

    //size 256 buffer - writable by processor thread, readable by ui. some weird frames expected.
    class AtomicRingBuf {
    public:
        AtomicRingBuf();

        bool isempty() { return (start.load() == end.load()); }
        bool isfull() { return (end.load() - start.load() == cap) || (end.load() - start.load() == -1); }

        //add int to end of ringbuf.  will do nothing if full.
        void add(int);
        //remove and return int from beginning of ringbuf
        //WILL RETURN IF EMPTY.  CHECK IFEMPTY() BEFORE CALLING
        int pop();

        //empty the ringbuf
        void clear() { end = start.load(); }

        //index of first element
        std::atomic<int> start;
        //index after last element
        std::atomic<int> end;

        //buffer 256 but cap of 255 so isempty(start==end) will work if full
        const std::atomic<int> cap = 255;
        std::atomic<int> data[256];
    };

    //AUTO GENERATED JUCE FUNCTIONS

    ProbCurvAudioProcessor();
    ~ProbCurvAudioProcessor();

    AudioProcessorValueTreeState::ParameterLayout createParameterLayout();

    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>&, MidiBuffer&) override;

    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    //setters and getters
    OutputMode getOutputMode() { return outputMode; }
    double getSampleRate() { return samplerate; }
    int getMaxPulses() { return pulse_Bank.cap; }
    int getSampsTillNextNote();

    //get ref to ringbuf, ONLY SAFE TO READ, NOT TO MODIFY
    AtomicRingBuf* getRingBuf() { return &pulse_Bank; }

    //sets refill flag, will refill at start of next buffer
    void queueRefill() { refillRequired = true; }

private:
    void addToNotebank(MidiMessage note);
    void removeFromNotebank(MidiMessage note);
    std::vector<MidiMessage>* getNotebank();

    //fills empty slots in lockfree pulse buffer with pulse durations
    void fillPulsebank();
    int popPulsebank();

    //sum of all the sliders that are currently drawn
    float sumSliders();

    //FLAGS

    //set when a parameter has changed that requires a pulsebank refill at the start of the next audio buffer
    std::atomic<bool> refillRequired;
    
    //STATE

    AudioProcessorValueTreeState state;

    //STATE parameter references

    //number of curve editor sliders set by the user
    std::atomic<float>* numSliders;
    //curveEditor x axis in seconds
    std::atomic<float>* curveTimescale;
    //values of curve editor sliders (points directly to the state value tree values)
    std::vector<std::atomic<float>*> sliderValues;

    //stores held notes
    std::vector<MidiMessage> notebank;

    //upcoming pulses stored atomically
    AtomicRingBuf pulse_Bank;

    //mode for how pulsebank should be converted to notes in buffer
    std::atomic<OutputMode> outputMode;

    //set random generators/seeds
    std::mt19937 generator;
    //distribution for generator
    const std::uniform_real_distribution<double> dist = std::uniform_real_distribution<double>(0, 1);

    //samplerate in samples/s
    std::atomic<double> samplerate;
    //duration of upcoming note in ms
    int nextNoteDur;
    //how many samples have elapsed in previous buffers
    int sampsElapsed;
    //queue of all notesOn messages that haven't had a corresponding noteoff sent
    std::queue<MidiMessage> notesOn;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ProbCurvAudioProcessor)
};
