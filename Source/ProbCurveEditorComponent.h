/*
  ==============================================================================

    ProbCurveEditorComponent.h
    Created: 20 Feb 2020 3:32:53pm
    Author:  gsuml

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include <vector>
#include "CurveEditorSlider.h"

//==============================================================================
/*
*/
class ProbCurveEditorComponent    : public Component, public AudioProcessorValueTreeState::Listener
{
public:
    ProbCurveEditorComponent(ProbCurvAudioProcessor &p, AudioProcessorValueTreeState& _state);
    ~ProbCurveEditorComponent();

    void resizeSliders(int newNumSliders);

    void parameterChanged(const String&, float) override;

    //mouseevents
    void mouseDrag(const MouseEvent& e) override;

    //graphics
    void paint (Graphics&) override;
    void resized() override;

private:
    ProbCurvAudioProcessor &processor;
    AudioProcessorValueTreeState& state;

    std::atomic<float>* numSliders;

    OwnedArray<CurveEditorSlider> sliders;
    std::vector<std::unique_ptr<AudioProcessorValueTreeState::SliderAttachment>> sliderAttachments;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ProbCurveEditorComponent)
};
