#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

//==============================================================================
/*
*/
class ProbCurveVisualizer    : public AnimatedAppComponent
{
public:
    ProbCurveVisualizer(ProbCurvAudioProcessor& p, AudioProcessorValueTreeState& _state);
    ~ProbCurveVisualizer();

    //reset drag state variables
    void mouseDown(const MouseEvent&) override;
    //update scale factor based on how far mouse has been dragged along Y axis.
    void mouseDrag(const MouseEvent&) override;

    void update() override;

    void paint (Graphics&) override;
    void resized() override;

private:
    ProbCurvAudioProcessor &processor;
    AudioProcessorValueTreeState &state;

    //reference to ringbuf.  can only safely do reads here
    ProbCurvAudioProcessor::AtomicRingBuf* ringbuf;
    //a few awkward frames due to lock free atomic reads are ok, but copying all the parameters
    //including ringbuf all at once results in much less stuttering of the visualizer over
    //reading from ringbuf directly during paint()
    std::vector<int> ringbufCopy;
    
    //other processor data
    int sampsTillNextNote;
    std::atomic<float>* curveTimescale;

    //width of visualizer in seconds
    float normalizedScaleFactor;
    float scaleFactor;

    //mouse gesture state
    float lastYOffset = 0;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ProbCurveVisualizer)
};
