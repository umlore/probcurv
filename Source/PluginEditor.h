/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

#include "ProbCurveEditorGUI.h"

//==============================================================================
/**
*/
class ProbCurvAudioProcessorEditor  : public AudioProcessorEditor
{
public:
    ProbCurvAudioProcessorEditor (ProbCurvAudioProcessor&, AudioProcessorValueTreeState& _state);
    ~ProbCurvAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    ProbCurvAudioProcessor& processor;

    AudioProcessorValueTreeState& state;

    ProbCurveEditorGUI *pcegui;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ProbCurvAudioProcessorEditor)
};
