#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
ProbCurvAudioProcessorEditor::ProbCurvAudioProcessorEditor (ProbCurvAudioProcessor& p, AudioProcessorValueTreeState& _state)
    : AudioProcessorEditor (&p), processor (p), state (_state)
{
    pcegui = new ProbCurveEditorGUI(p, state);

    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (600, 400);

    addAndMakeVisible(pcegui);
}

ProbCurvAudioProcessorEditor::~ProbCurvAudioProcessorEditor()
{
    delete pcegui;
}

//==============================================================================
void ProbCurvAudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll(Colours::floralwhite);

    g.setColour (Colours::cadetblue);
    g.setFont (15.0f);
    g.drawFittedText ("Probability Curve", 0, 0, getWidth(), 30, Justification::centred, 1);
}

void ProbCurvAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..

    pcegui->setBounds(0, 0, getWidth(), getHeight());
}
