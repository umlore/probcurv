#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "ProbCurveEditorComponent.h"

//==============================================================================
ProbCurveEditorComponent::ProbCurveEditorComponent(ProbCurvAudioProcessor &p, AudioProcessorValueTreeState& _state) : processor(p), state(_state)
{
    numSliders = state.getRawParameterValue("numSliders");

    sliders = OwnedArray<CurveEditorSlider>();

    //create array of sliders invisible
    NormalisableRange<float> numSlidersRange = state.getParameterRange("numSliders");
    for (int i = 0; i < numSlidersRange.end; i++) {
        CurveEditorSlider* slider = sliders.add(new CurveEditorSlider(std::to_string(i)));
        slider->setSliderStyle(juce::Slider::LinearVertical);
        slider->setTextBoxStyle(Slider::TextEntryBoxPosition::TextBoxBelow, true, 100, 50);

        auto name = "ceSlider" + std::to_string(i);
        sliderAttachments.push_back(nullptr);
        sliderAttachments.back().reset(new AudioProcessorValueTreeState::SliderAttachment(state, name, *slider));
        state.addParameterListener(name, this);

        addChildComponent(slider);
    }

    //use setVisible in resizeSliders to determine if they are rendered if in bound
    resizeSliders(*numSliders);
}

ProbCurveEditorComponent::~ProbCurveEditorComponent()
{
    for (int i = 0; i < sliderAttachments.size(); i++) {
        auto name = "ceSlider" + std::to_string(i);
        state.removeParameterListener(name, this);
    }
}

//MUST be called after constructing or no sliders will render
//sets visibility of all sliders based on total number to be drawn, then calls resized
void ProbCurveEditorComponent::resizeSliders(int newNumSliders) {
    for (int i = 0; i < *numSliders; i++) {
        sliders[i]->setVisible(true);
    }
    for (int i = *numSliders; i < state.getParameterRange("numSliders").end; i++) {
        sliders[i]->setVisible(false);
    }

    resized();
}

void ProbCurveEditorComponent::parameterChanged(const String& parameterID, float newValue)
{
    //if numSliders changes, redraw the curve editor sliders
    //Use MessageManager to call component functions, for thread safety
    MessageManager* mm = MessageManager::getInstance();
    if (parameterID == "numSliders")
        mm->callAsync([&] {resizeSliders(newValue); });

    //schedule processor to refill pulseBank
    //curveEditorSliders will also use this callback to reset the pulsebank
    processor.queueRefill();
}

void ProbCurveEditorComponent::mouseDrag(const MouseEvent& e)
{
    //index of slider hitbox mouse is over given mouse x position
    int sliderMouseOverIndex = jlimit(0, (int)*numSliders - 1, (int) floor(e.position.x / (getWidth() / ((double)(*numSliders)))));
    //value to set slider given mouse y position
    double slidervalue = ((double)(getHeight() - sliders[sliderMouseOverIndex]->getTextBoxHeight() - e.position.y)) / ((double)(getHeight() - sliders[sliderMouseOverIndex]->getTextBoxHeight()));
    sliders[sliderMouseOverIndex]->setValue(slidervalue);
}

void ProbCurveEditorComponent::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background

    g.setColour (Colours::grey);
    g.drawRect (getLocalBounds(), 1);   // draw an outline around the component
}

void ProbCurveEditorComponent::resized()
{
    //sliders evenly divide width of parent
    for (int i = 0; i < *numSliders; i++) {
        int xOff = i * getWidth() / *numSliders;
        sliders[i]->setBounds(xOff, 0, (i + 1) * getWidth() / *numSliders - xOff, getHeight());
    }
}
