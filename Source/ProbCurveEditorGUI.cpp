/*
  ==============================================================================

    ProbCurveEditorGUI.cpp
    Created: 19 Feb 2020 5:33:17pm
    Author:  gsuml

  ==============================================================================
*/

#include <JuceHeader.h>
#include "ProbCurveEditorGUI.h"

//==============================================================================
ProbCurveEditorGUI::ProbCurveEditorGUI(ProbCurvAudioProcessor& p, AudioProcessorValueTreeState& _state) : processor (p), state (_state)
{
    pceComponent = new ProbCurveEditorComponent(processor, state);
    pcViz = new ProbCurveVisualizer(processor, state);

    makenumSlidersSlider();
    makecurveTimescaleSlider();

    addAndMakeVisible(pcViz);
    addAndMakeVisible(pceComponent);
}

ProbCurveEditorGUI::~ProbCurveEditorGUI()
{
    state.removeParameterListener("numSliders", pceComponent);
    state.removeParameterListener("curveTimescale", pceComponent);
    delete pcViz;
    delete pceComponent;
}

void ProbCurveEditorGUI::makenumSlidersSlider() {
    //set label style/content
    numSlidersLabel.setText("# Curve Sliders", dontSendNotification);
    numSlidersLabel.attachToComponent(&numSlidersSlider, false);
    numSlidersLabel.setJustificationType(Justification::centred);

    //set slider style
    numSlidersSlider.setSliderStyle(Slider::Rotary);
    numSlidersSlider.setTextBoxStyle(Slider::TextEntryBoxPosition::TextBoxBelow, false, 90, 20);
    numSlidersSlider.setPopupDisplayEnabled(true, true, this);

    //apvts callbacks
    numSlidersAttachment.reset(new SliderAttachment(state, "numSliders", numSlidersSlider));
    state.addParameterListener("numSliders", pceComponent);

    addAndMakeVisible(numSlidersSlider);
}

void ProbCurveEditorGUI::makecurveTimescaleSlider() {
    //set label style/content
    curveTimescaleLabel.setText("Timescale", dontSendNotification);
    curveTimescaleLabel.attachToComponent(&curveTimescaleSlider, false);
    curveTimescaleLabel.setJustificationType(Justification::centred);

    //set fields
    double initCurveDur = processor.getSampleRate();
    curveTimescaleSlider.setSliderStyle(Slider::Rotary);
    curveTimescaleSlider.setTextBoxStyle(Slider::TextEntryBoxPosition::TextBoxBelow, false, 90, 20);
    curveTimescaleSlider.setPopupDisplayEnabled(true, true, this);
    curveTimescaleSlider.setTextValueSuffix(" s");

    //apvts functionality
    curveTimescaleAttachment.reset(new SliderAttachment(state, "curveTimescale", curveTimescaleSlider));
    state.addParameterListener("curveTimescale", pceComponent);

    //curveTimescaleSlider.addListener(this);
    addAndMakeVisible(curveTimescaleSlider);
}

void ProbCurveEditorGUI::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background

    g.setColour (Colours::grey);
    g.drawRect (getLocalBounds(), 1);   // draw an outline around the component
}

void ProbCurveEditorGUI::resized()
{
    //left column sliders
    int sliderWidth = 100;
    int labelMargin = 20;
    numSlidersSlider.setBounds(0, labelMargin * 3, sliderWidth, sliderWidth);
    curveTimescaleSlider.setBounds(0, sliderWidth + labelMargin * 7, sliderWidth, sliderWidth);

    //visualizer
    int vizHeight = 100;
    pcViz->setBounds(sliderWidth, 0, getWidth() - sliderWidth, vizHeight);

    //curve editor
    pceComponent->setBounds(sliderWidth, vizHeight, getWidth() - sliderWidth, getHeight() - vizHeight);
}
