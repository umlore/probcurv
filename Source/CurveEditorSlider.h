#pragma once

#include <JuceHeader.h>

//==============================================================================
class CurveEditorSlider    : public Slider
{
public:
    CurveEditorSlider(const String& name) : Slider (name)
    {
    }

    ~CurveEditorSlider()
    {
    }

    void mouseDrag(const MouseEvent& e) override {
        getParentComponent()->mouseDrag(e.getEventRelativeTo(getParentComponent()));
    }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CurveEditorSlider)
};
