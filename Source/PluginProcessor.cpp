#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
ProbCurvAudioProcessor::ProbCurvAudioProcessor()
    : state(*this, nullptr, Identifier("ProbCurv"), createParameterLayout())
{
    //set members
    refillRequired = false;
    outputMode = OutputMode::chords;

    //set random generator seeds
    std::random_device rd;
    generator = std::mt19937(rd());

    //set pointer accessers to parameter state
    numSliders = state.getRawParameterValue("numSliders");
    curveTimescale = state.getRawParameterValue("curveTimescale");
    for (int i = 0; i < state.getParameterRange("numSliders").end; i++) {
        sliderValues.push_back(state.getRawParameterValue("ceSlider" + std::to_string(i)));
    }
}

ProbCurvAudioProcessor::~ProbCurvAudioProcessor()
{
}

AudioProcessorValueTreeState::ParameterLayout ProbCurvAudioProcessor::createParameterLayout()
{
    int numSliderMax = 60;

    //create single slider parameters
    AudioProcessorValueTreeState::ParameterLayout layout = AudioProcessorValueTreeState::ParameterLayout({
        std::make_unique<AudioParameterFloat>("numSliders", "NumSliders", NormalisableRange<float>(1, numSliderMax, 1, .5, false), 10),
        std::make_unique<AudioParameterFloat>("curveTimescale", "Timescale", NormalisableRange<float>(.5, 60, .01, .3, false), 1)
    });
    
    //add the curve editor sliders array
    for (int i = 0; i < numSliderMax; i++) {
        layout.add(std::make_unique<AudioParameterFloat>("ceSlider" + std::to_string(i), "CurveEditorSlider" + std::to_string(i), 0, 1, 0));
    }

    return layout;
}

//==============================================================================
const String ProbCurvAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool ProbCurvAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool ProbCurvAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool ProbCurvAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double ProbCurvAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int ProbCurvAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int ProbCurvAudioProcessor::getCurrentProgram()
{
    return 0;
}

void ProbCurvAudioProcessor::setCurrentProgram (int index)
{
}

const String ProbCurvAudioProcessor::getProgramName (int index)
{
    return {};
}

void ProbCurvAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void ProbCurvAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..

    samplerate = sampleRate;
    sampsElapsed = 0;
    nextNoteDur = 0;
}

void ProbCurvAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool ProbCurvAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void ProbCurvAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    int time;
    MidiMessage m;

    //keep a running list of which notes are included in the notebank
    for (MidiBuffer::Iterator i(midiMessages); i.getNextEvent(m, time);) {
        if (m.isNoteOn()) {
            addToNotebank(m);
        }
        else if (m.isNoteOff()) {
            removeFromNotebank(m);
        }
    }

    //todo: currently noteoffs will only happen when a new note on is triggered.
    //when note length is configured, make sure this check doesn't bypass noteoff
    //if sumSliders() is 0, no notes should be sent, so don't do the rest of the state calculations.
    if (sumSliders() <= 0) {
        return;
    }

    int buffersize = buffer.getNumSamples();

    //check to see if a parameter has updated that would require a refill
    if (refillRequired) {
        pulse_Bank.clear();
        refillRequired = false;
    }

    fillPulsebank();

    //if nextNoteDur hasn't been set or pulls a no-duration note, grab a new value
    while (nextNoteDur == 0) {
        nextNoteDur = popPulsebank();
    }

    int offset = 0;

    //if a note should be triggered in this buffer
    while (sampsElapsed + buffersize - offset >= nextNoteDur) {
        //set start position in the buffer (-1 because sampsElapsed is 0 indexed and noteDuration is 1 indexed)
        offset += nextNoteDur - sampsElapsed - 1;
        sampsElapsed = 0;

        //offset must be within the size of the buffer
        jassert(offset < buffersize);
        
        //send noteoff message for all noteson
        while (!notesOn.empty()) {
            MidiMessage note = notesOn.front();
            notesOn.pop();
            midiMessages.addEvent(MidiMessage::noteOff(note.getChannel(), note.getNoteNumber()), offset);
        }

        //depending on the output mode:
        if (getOutputMode() == ProbCurvAudioProcessor::OutputMode::chords) {
            //send all held notes at once
            std::vector<MidiMessage>* notebank = getNotebank();
            for (int i = 0; i < notebank->size(); i++) {
                MidiMessage note = MidiMessage::noteOn((*notebank)[i].getChannel(), (*notebank)[i].getNoteNumber(), (*notebank)[i].getVelocity());
                midiMessages.addEvent(note, offset);
                notesOn.push(note);
            }
        }
        //todo: other output modes

        nextNoteDur = popPulsebank();
    }

    //update samps elapsed
    sampsElapsed += buffersize - offset;
}

//==============================================================================
bool ProbCurvAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* ProbCurvAudioProcessor::createEditor()
{
    return new ProbCurvAudioProcessorEditor (*this, state);
}

//==============================================================================
void ProbCurvAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    auto parameterstate = state.copyState();
    std::unique_ptr<XmlElement> xml(parameterstate.createXml());
    copyXmlToBinary(*xml, destData);
}

void ProbCurvAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    std::unique_ptr<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));

    if (xmlState.get() != nullptr)
        if (xmlState->hasTagName(state.state.getType()))
            state.replaceState(ValueTree::fromXml(*xmlState));
}

int ProbCurvAudioProcessor::getSampsTillNextNote()
{
    jassert(MessageManager::existsAndIsLockedByCurrentThread());
    return nextNoteDur - sampsElapsed;
}

//add to held notes
void ProbCurvAudioProcessor::addToNotebank(MidiMessage note)
{
    notebank.push_back(note);
}

//remove from held notes
void ProbCurvAudioProcessor::removeFromNotebank(MidiMessage note)
{
    auto it = notebank.begin();
    while (it != notebank.end()) {
        if (it->getNoteNumber() == note.getNoteNumber()) {
            notebank.erase(it);
            break;
        }
        it++;
    }
}

std::vector<MidiMessage>* ProbCurvAudioProcessor::getNotebank()
{
    return &notebank;
}

void ProbCurvAudioProcessor::fillPulsebank() {
    //if there's nothing to add, don't bother calculating.
    if (pulse_Bank.isfull()) {
        return;
    }

    //get sum of sliderValues in range
    float sliderssum = sumSliders();

    //calculate percent of notes that should fall under each curve sample
    std::vector<double> samplePercentages(*numSliders);

    //sanity check that we have not set more sliders than sliderValues
    jassert(*numSliders <= sliderValues.size());

    //base case for sP[0]
    if (sliderValues.size() > 0) {
        samplePercentages[0] = *sliderValues[0] / sliderssum;
        if (sliderssum == 0) { //divide by zero case
            samplePercentages[0] = 0;
        }
    }

    //sP[n] defined as (% of notes to trigger within slider n's time range) + sP[n-1]
    for (int i = 1; i < *numSliders; i++) {
        samplePercentages[i] = (*sliderValues[i] / sliderssum) + samplePercentages[i - 1];
        if (sliderssum == 0) { //divide by zero case
            samplePercentages[i] = 0;
        }
    }
    //todo: test that we're getting within an epsilon of 1 (100%) with these calculations.
    //unless if sliderssum is zero, should be zero
    //jassert(samplePercentages[samplePercentages.size() - 1] >= 1.);

    //generate notes until pulsebank is full
    while (!pulse_Bank.isfull()) {
    //while (pulseBank.size() < maxPulses) {

        //get a random seed for the next note, representing a percentage
        double seed = dist(generator);

        int nextdur = 0;

        //check which slider's time range seed should fall within
        for (int i = 0; i < samplePercentages.size(); i++) {
            //if seed is set such that the note should fall within this slider's time range:
            if (seed <= samplePercentages[i]) {
                //nextdur = number of sliders before + percent of this slider volume (interpolated linearly) / number of sliders * total time of the curve
                //or, time of the curve * percent volume of the curve underneath the value of seed
                if (i == 0) {
                    nextdur = ((((double)i) + (seed / (samplePercentages[i]))) / *numSliders) * (*curveTimescale * samplerate);
                }
                else {
                    nextdur = ((((double)i) + ((seed - samplePercentages[i - 1]) / (samplePercentages[i] - samplePercentages[i - 1]))) / *numSliders) * (*curveTimescale * samplerate);
                }

                break;
            }
        }

        //if a note duration hasn't been properly generated, it should be the maximum possible length
        //this is done for one of two reasons:
        //      epsilon error means that sample percentages covers < 100% of cases and a VERY high seed
        //      or sliderssum == 0, and we want to reduce the number of calculations done as much as possible because notes shouldn't be sent anyway.
        //it should not be possible to set nextdur to 0 intentionally because that could cause infinite looping in ProcessBlock().
        if (nextdur == 0) {
            nextdur = *curveTimescale * samplerate;
        }

        pulse_Bank.add(nextdur);
        //pulseBank.push_back(nextdur);
    }
}

int ProbCurvAudioProcessor::popPulsebank() {
    if (!pulse_Bank.isempty()) {
        return pulse_Bank.pop();
    }
    else {
        fillPulsebank();
        return popPulsebank();
    }
}

float ProbCurvAudioProcessor::sumSliders()
{
    float sum = 0;
    for (int i = 0; i < *numSliders; i++) {
        sum += *sliderValues[i];
    }
    return sum;
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new ProbCurvAudioProcessor();
}




//ringbuf
ProbCurvAudioProcessor::AtomicRingBuf::AtomicRingBuf() {
    start = 0;
    end = 0;
    for (int i = 0; i < cap + 1; i++) {
        data[i] = 0;
    }
}

void ProbCurvAudioProcessor::AtomicRingBuf::add(int val) {
    if (!isfull()) {
        if (end.load() == cap + 1) {
            end = 0;
        }
        data[end] = val;
        end++;
    }
}

int ProbCurvAudioProcessor::AtomicRingBuf::pop() {
    int val = data[start].load();
    start++;
    if (start == cap + 1) {
        start = 0;
    }
    return val;
}